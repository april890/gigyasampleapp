package com.example.pactera.firstgigyaapp;

public interface ICommand {
    void execute();
}

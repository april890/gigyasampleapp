package com.example.pactera.firstgigyaapp.ui.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pactera.firstgigyaapp.R;
import com.example.pactera.firstgigyaapp.model.User;
import com.example.pactera.firstgigyaapp.ui.UIResponse;
import com.example.pactera.firstgigyaapp.ui.viewmodel.AccountViewModel;

public class AccountActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "AccountActivity";
    private TextInputLayout mFirstNameWrapper;
    private TextInputLayout mLastNameWrapper;

    private TextView mProgressText;

    private LinearLayout mProgressLayout;
    private AccountViewModel mAccountViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mAccountViewModel = ViewModelProviders.of(this).get(AccountViewModel.class);
        initView();
        setObserver();
        getUserData();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //validateViews();
    }

    private void initView() {
        mProgressLayout = findViewById(R.id.progress_bar_layout);
        mFirstNameWrapper = findViewById(R.id.firstName_input_wrapper);
        mLastNameWrapper = findViewById(R.id.lastName_input_wrapper);
        mProgressText = findViewById(R.id.progress_bar_message);

        Button btnUpdate = findViewById(R.id.button_update);
        Button btnLogout = findViewById(R.id.button_logout);

        btnUpdate.setOnClickListener(this);
        btnLogout.setOnClickListener(this);

    }

    private void setObserver() {
        mAccountViewModel.getLogoutLiveData().observe(this, new Observer<UIResponse>() {
            @Override
            public void onChanged(@Nullable UIResponse uiResponse) {
                switch (uiResponse.getResponseType()) {
                    case SUCCESS:
                        hideProgressBar();
                        finish();
                        break;

                    case IN_PROGRESS:
                        showProgressBar(R.string.dialog_logging_out);
                        break;

                    case ERROR:
                        hideProgressBar();
                        break;
                }
            }
        });

        mAccountViewModel.getAccountLiveData().observe(this, new Observer<UIResponse>() {

            @Override
            public void onChanged(@Nullable UIResponse uiResponse) {
                switch (uiResponse.getResponseType()) {
                    case SUCCESS:
                        hideProgressBar();
                        if (uiResponse.getData() != null) {
                            User u = (User) uiResponse.getData();
                            mFirstNameWrapper.getEditText().setText(u.getFirstName());
                            mLastNameWrapper.getEditText().setText(u.getLastName());
                        }else{
                            Toast toast = Toast.makeText(AccountActivity.this, R.string.update_account_success_message, Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                            toast.show();
                        }
                        break;

                    case IN_PROGRESS:
                        showProgressBar(R.string.dialog_updating_user_data);
                        break;

                    case ERROR:
                        hideProgressBar();
                        Toast.makeText(AccountActivity.this, "Error", Toast.LENGTH_LONG).show();
                        break;
                }
            }
        });

        mAccountViewModel.getPurchaseLiveData().observe(this, new Observer<UIResponse>() {
            @Override
            public void onChanged(@Nullable UIResponse uiResponse) {
                switch (uiResponse.getResponseType()) {
                    case SUCCESS:
                        hideProgressBar();
                        if (uiResponse.getData() != null) {
                            String message = (String) uiResponse.getData();
                            Toast toast = Toast.makeText(AccountActivity.this, message, Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                            toast.show();
                        }
                        break;

                    case IN_PROGRESS:
                        showProgressBar(R.string.dialog_purchase_item);
                        break;

                    case ERROR:
                        hideProgressBar();
                        Toast.makeText(AccountActivity.this, "Invalid User!", Toast.LENGTH_LONG).show();
                        break;
                }
            }
        });
    }

    private void showProgressBar(int messageID) {
        mProgressText.setText(messageID);
        mProgressLayout.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        mProgressLayout.setVisibility(View.GONE);
    }

    private void getUserData() {
        mAccountViewModel.getAccountDetails();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_update:
                if (isValidInput()) {
                    //mFirstNameWrapper.setErrorEnabled(false);
                   // mLastNameWrapper.setErrorEnabled(false);
                    String firstName = null;
                    String lastName = null;
                    Editable firstNameEditable = mFirstNameWrapper.getEditText().getText();
                    Editable lastNameEditable = mLastNameWrapper.getEditText().getText();
                    if (firstNameEditable != null) {
                        firstName = firstNameEditable.toString();
                    }
                    if (lastNameEditable != null) {
                        lastName = lastNameEditable.toString();
                    }
                    mAccountViewModel.setAccountDetails(firstName, lastName);
                } else {
                    mFirstNameWrapper.setError("First name is not valid!");
                    mLastNameWrapper.setError("Last name is not valid!");
                    //TODO: invalid input
                }
                break;

            case R.id.button_logout:
                mAccountViewModel.logout();
                break;

        }
    }

    /**
     * Validating that there is at least one input field with no empty value
     *
     * @return
     */
    private boolean isValidInput() {

        Editable firstNameEditable = mFirstNameWrapper.getEditText().getText();
        Editable lastNameEditable = mLastNameWrapper.getEditText().getText();
        String firstName = null;
        String lastName = null;

        if (firstNameEditable != null) {
            firstName = firstNameEditable.toString();
        }
        if (lastNameEditable != null) {
            lastName = lastNameEditable.toString();
        }

        if (TextUtils.isEmpty(firstName) || TextUtils.isEmpty(lastName)) {
            return false;
        }
        return true;
    }

}

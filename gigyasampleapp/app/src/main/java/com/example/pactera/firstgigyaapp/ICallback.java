package com.example.pactera.firstgigyaapp;

public interface ICallback<R, E extends Throwable> {

    void onSuccess(R result);
    void onError(E error);
}

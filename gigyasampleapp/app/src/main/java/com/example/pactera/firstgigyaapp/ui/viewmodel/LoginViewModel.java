package com.example.pactera.firstgigyaapp.ui.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.arch.lifecycle.ViewModel;

import com.example.pactera.firstgigyaapp.IGigyaEvents;
import com.example.pactera.firstgigyaapp.managers.RegistrationManager;
import com.example.pactera.firstgigyaapp.ui.UIResponse;

public class LoginViewModel extends AndroidViewModel {
    private static final String TAG = "LoginViewModel";
    private MutableLiveData<UIResponse> mLoginLiveData;


    public LoginViewModel(@NonNull Application application) {
        super(application);
    }

    public void initialize(String apiKey, String dataCenter){
        RegistrationManager.getInstance().initialize(getApplication(), apiKey, dataCenter);
    }


    public MutableLiveData<UIResponse> getLoginLiveData(){
        if(mLoginLiveData == null){
            mLoginLiveData = new MutableLiveData<>();
        }
        return mLoginLiveData;
    }

    /**
     * Native Login
     * Calling to the show login UI API of the gigya SDK
     */
    public void showLoginUI(){
        mLoginLiveData.postValue(UIResponse.IN_PROGRESS());
        RegistrationManager.getInstance().addObserver(new IGigyaEvents() {
            @Override
            public void onEvent(EventType eventType) {
                RegistrationManager.getInstance().removeObserver(this);
                if(eventType == EventType.LOGIN){
                    mLoginLiveData.postValue(UIResponse.SUCCESS(null));
                }else{
                    mLoginLiveData.postValue(UIResponse.ERROR("Logout Failed"));
                }
            }
        });
        RegistrationManager.getInstance().showUI();
    }

    /**
     * RaaS login using screen sets
     */
    public void showPlugin(String prefix, String language){
        mLoginLiveData.postValue(UIResponse.IN_PROGRESS());
        RegistrationManager.getInstance().addObserver(new IGigyaEvents() {
            @Override
            public void onEvent(EventType eventType) {
                RegistrationManager.getInstance().removeObserver(this);
                if(eventType == EventType.LOGIN){
                    mLoginLiveData.postValue(UIResponse.SUCCESS(null));
                }else{
                    mLoginLiveData.postValue(UIResponse.ERROR("Logout Failed"));
                }
            }
        });
        RegistrationManager.getInstance().showPlugin(prefix, language);
    }


    public boolean isSecured(){
        return RegistrationManager.getInstance().isSecured();
    }
}

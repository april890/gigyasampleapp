package com.example.pactera.firstgigyaapp.network.commands;

import com.example.pactera.firstgigyaapp.ICallback;
import com.example.pactera.firstgigyaapp.ICommand;

import com.example.pactera.firstgigyaapp.GigyaSampleLog;
import com.example.pactera.firstgigyaapp.R;
import com.example.pactera.firstgigyaapp.model.User;
import com.gigya.socialize.GSObject;
import com.gigya.socialize.GSResponse;
import com.gigya.socialize.GSResponseListener;
import com.gigya.socialize.android.GSAPI;

public class SetAccountInfoCommand implements ICommand {

    private static final String TAG = "SetAccountInfoCommand";
    private static final String METHOD_NAME = "accounts.setAccountInfo";
    private ICallback<String, Exception> mCallback;
    private String mFirstName;
    private String mLastName;
    private String mUid;


    public SetAccountInfoCommand(String uid, String firstName, String lastName, ICallback callback) {
        mFirstName = firstName;
        mLastName = lastName;
        mUid = uid;
        mCallback = callback;
    }


    @Override
    public void execute() {
        final GSResponseListener resListener = new GSResponseListener() {
            @Override
            public void onGSResponse(String method, GSResponse response, Object context) {
                try {
                    if (response.getErrorCode() == 0) { // SUCCESS! response status = OK
                        /**
                         * On Success
                         */
                        GigyaSampleLog.i(TAG, "----setUserInfo success----");
                        mCallback.onSuccess(response.getResponseText());
                    } else
                        {
                        GigyaSampleLog.i(TAG, "Error: in setUserInfo operation. " + response.toString());
                        int errorCode = response.getErrorCode();
                        String errorMessage = response.getErrorMessage();
                        mCallback.onError(new Exception(errorCode+errorMessage));
                    }
                } catch (Exception ex) {
                    mCallback.onError(ex);
                }
            }
        };

        GSObject profileInfo = new GSObject();
        profileInfo.put("firstName", mFirstName);
        profileInfo.put("lastName", mLastName);

        GSObject accountInfo = new GSObject();
        accountInfo.put("UID", mUid);

        accountInfo.put("profile", profileInfo);

        GSAPI.getInstance().sendRequest(METHOD_NAME, accountInfo, resListener, null);
    }
}

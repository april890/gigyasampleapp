package com.example.pactera.firstgigyaapp.ui.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.pactera.firstgigyaapp.R;
import com.example.pactera.firstgigyaapp.ui.UIResponse;
import com.example.pactera.firstgigyaapp.ui.viewmodel.LoginViewModel;


public class LoginActivity extends AppCompatActivity {


    private LoginViewModel mLoginViewModel;
    private LinearLayout mProgressLayout;

    public LoginActivity() {
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mProgressLayout = findViewById(R.id.login_progress_bar_layout);
        mLoginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        initGigyaLogin();
        setObserver();

    }

    private void initGigyaLogin() {
        String apiKey = getResources().getString(R.string.api_key);
        String dataCenter = getResources().getString(R.string.default_data_center);
        mLoginViewModel.initialize(apiKey, dataCenter);
    }

    @Override
    protected void onResume() {
        initView();
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void initView() {
        String editLang = getResources().getString(R.string.default_screen_set_language);
        String editPrefix = getResources().getString(R.string.default_screen_set_prefix);
        //mProgressLayout.setVisibility(View.GONE);
        mLoginViewModel.showPlugin(editPrefix, editLang);


    }

    private void setObserver() {
        mLoginViewModel.getLoginLiveData().observe(this, new Observer<UIResponse>() {
            @Override
            public void onChanged(@Nullable UIResponse uiResponse) {
                switch (uiResponse.getResponseType()) {
                    case SUCCESS:
                        showAccountActivity();
                        break;

                    case IN_PROGRESS:
                        //TODO: Showing IN_PROGRESS indicator
                        break;

                    case ERROR:
                        Toast.makeText(LoginActivity.this, R.string.logout_error_message, Toast.LENGTH_LONG).show();
                        break;
                }
            }
        });
    }


    /**
     *
     */
    private void showAccountActivity() {
       Intent accountIntent = new Intent(this, AccountActivity.class);
       startActivity(accountIntent);
    }
}

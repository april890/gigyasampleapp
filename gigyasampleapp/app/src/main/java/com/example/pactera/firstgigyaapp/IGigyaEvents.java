package com.example.pactera.firstgigyaapp;

public interface IGigyaEvents {

    enum EventType {LOGIN, LOGOUT, LOAD, CLOSE, ERROR}

    void onEvent(EventType eventType);
}
